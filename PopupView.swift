//
//  PopupView.swift
//  OpenCV
//
//  Created by Federico Parroni on 17/04/17.
//  Copyright © 2017 Federico Parroni. All rights reserved.
//

import UIKit


@IBDesignable class PopupView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var borderColor : UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
            setNeedsDisplay()
        }
    }
    
    @IBInspectable var borderWidth : Float = 0 {
        didSet {
            layer.borderWidth = CGFloat(borderWidth)
            setNeedsDisplay()
        }
    }

    @IBInspectable var cornerRadius : Float = 0.0 {
        didSet {
            layer.cornerRadius = CGFloat(cornerRadius)
            setNeedsDisplay()
        }
    }
    
}
