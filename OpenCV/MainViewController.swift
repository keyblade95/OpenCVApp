//
//  ViewController.swift
//  OpenCV
//
//  Created by Federico Parroni on 14/04/17.
//  Copyright © 2017 Federico Parroni. All rights reserved.
//

import UIKit
import AVFoundation

class MainViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    @IBOutlet var popupView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var drawSwitch: UISwitch!
    
    
    var dictionaryNames : Array<Any> = []
    var camMatrix : Array<Double> = []
    var distCoeffs : Array<Double> = []
    
    
    let captureSession = AVCaptureSession()
    var previewLayer : AVCaptureVideoPreviewLayer?
    var captureDevice : AVCaptureDevice?
    var videoQueue : DispatchQueue?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        camMatrix = [7.4375781056863980e+02, 0, 4.4855147802229965e+02,
                         0, 7.1058541708876464e+02, 3.2895498545114140e+02,
                         0, 0, 1]
        distCoeffs = [3.9271136511602989e-01, -5.1462902587370585e-01, 6.8541405781738420e-03, 1.1727174954919782e-01, 3.3015051143446644e-01]
        
        dictionaryNames = OpenCVWrapper.getDictionaries()
        pickerView.selectRow(10, inComponent: 0, animated: false)
        
        // get camera frame
        StartCameraStream()
        
    }
    
//    MARK: - CAMERA SESSION
    func StartCameraStream() {
        captureSession.sessionPreset = AVCaptureSessionPresetMedium
        
        captureDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back)
        if captureDevice != nil {
            
            do {
                try captureSession.addInput(AVCaptureDeviceInput(device: captureDevice))
            } catch let err as NSError {
                print("Error while adding input to capture session")
                print(err.localizedDescription)
            }
            
            let videoOutput = AVCaptureVideoDataOutput()
            //videoOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as String) : kCVPixelFormatType_24RGB]
            videoOutput.alwaysDiscardsLateVideoFrames = true
            videoQueue = DispatchQueue(label: "videoCapture")
            videoOutput.setSampleBufferDelegate(self, queue: videoQueue)
            captureSession.addOutput(videoOutput)
            
            videoOutput.connection(withMediaType: AVMediaTypeVideo)!.videoOrientation = .portrait
            
//            if !drawSwitch.isOn {
//                // add PREVIEW frame to preview view
//                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
//                previewLayer?.connection.videoOrientation = .portrait
//                previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
//                previewLayer!.frame = imageView.bounds
//                imageView.layer.addSublayer(previewLayer!)
//            }
        
            captureSession.startRunning()
        } else {
            print("ERROR: Capture device not found!\n")
        }
    }
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {

        let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
        // convert pixelbuffer into UIImage
        let ciImg = CIImage(cvPixelBuffer: pixelBuffer)
        let context = CIContext(options: nil)
        let cgImg = context.createCGImage(ciImg, from: CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(pixelBuffer), height: CVPixelBufferGetHeight(pixelBuffer)))
        let uiImg = UIImage(cgImage: cgImg!)
        
        DetectMarkers(img: uiImg)
    }
    
    func DetectMarkers(img : UIImage) {
        let dict = pickerView.selectedRow(inComponent: 0)
        let result : ArucoData = OpenCVWrapper.detectMarkersAndEstimatePose(img, ofDictionary: Int32(dict), camMatrix: camMatrix, distCoeffs: distCoeffs, shouldDraw: drawSwitch.isOn)
        
        DispatchQueue.main.async {
            self.lbl.text = "Markers: \(result.markerIds.count)"
            self.imageView.image = result.image
        }
    }
    
    
//    MARK: - UI ACTIONS
    @IBAction func settingsTapped(_ sender: UIBarButtonItem) {
        ShowPopup()
    }
    func ShowPopup() {
        
        popupView.center = view.center
        popupView.alpha = 0
        popupView.transform = CGAffineTransform(scaleX: 0.8, y: 1.2)
        view.addSubview(popupView)
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            
            self.popupView.alpha = 1
            self.popupView.transform = .identity
        })
        
    }
    func DismissPopup() {
        popupView.removeFromSuperview()
    }
    
    @IBAction func viewTapped(_ sender: UITapGestureRecognizer) {
        DismissPopup()
    }

}
