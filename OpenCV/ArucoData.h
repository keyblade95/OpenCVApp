//
//  ArucoData.m
//  OpenCV
//
//  Created by Federico Parroni on 18/04/17.
//  Copyright © 2017 Federico Parroni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArucoData : NSObject

@property NSArray *markerIds;
@property UIImage *image;

@end
