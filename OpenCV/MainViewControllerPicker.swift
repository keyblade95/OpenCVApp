//
//  MainViewControllerPickerView.swift
//  OpenCV
//
//  Created by Federico Parroni on 18/04/17.
//  Copyright © 2017 Federico Parroni. All rights reserved.
//

import UIKit

extension MainViewController : UIPickerViewDataSource, UIPickerViewDelegate {

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dictionaryNames.count
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(dictionaryNames[row])"
    }

}
