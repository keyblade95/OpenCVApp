//
//  OpenCVWrapper.m
//  OpenCV
//
//  Created by Federico Parroni on 14/04/17.
//  Copyright © 2017 Federico Parroni. All rights reserved.
//

#import "OpenCVWrapper.h"
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#import <opencv2/aruco.hpp>

using namespace std;
using namespace cv;


@implementation OpenCVWrapper

+(NSString *) OpenCVVersionString {
    return [NSString stringWithFormat:@"Version: %s", CV_VERSION];
}

+ (NSArray *) getDictionaries {
    
    NSArray *res = [NSArray arrayWithObjects:@"DICT_4X4_50", @"DICT_4X4_100", @"DICT_4X4_250", @"DICT_4X4_1000",
                                            @"DICT_5X5_50", @"DICT_5X5_100", @"DICT_5X5_250", @"DICT_5X5_1000",
                                            @"DICT_6X6_50", @"DICT_6X6_100", @"DICT_6X6_250", @"DICT_6X6_1000",
                                            @"DICT_7X7_50", @"DICT_7X7_100", @"DICT_7X7_250", @"DICT_7X7_1000",
                                            @"DICT_ARUCO_ORIGINAL", nil ];
    return res;
}


+(ArucoData *) detectMarkersAndEstimatePose:(UIImage *)img ofDictionary:(int)dictionary camMatrix:(NSArray *)camMatrix distCoeffs:(NSArray *)distCoeffs shouldDraw:(bool)drawMarkers {
    
    Mat inputImg;
    UIImageToMat(img, inputImg);
    
    // remove alpha channel if present
    if(inputImg.channels() == 4) {
        cvtColor(inputImg, inputImg, CV_BGRA2BGR);
    }
    
    Ptr<aruco::Dictionary> dict = aruco::getPredefinedDictionary(dictionary);
    
    vector<int> markerIds;
    vector<vector<Point2f>> corners, rejectedCandidates;
    Ptr<aruco::DetectorParameters> parameters = cv::aruco::DetectorParameters::create();
    
    aruco::detectMarkers(inputImg, dict, corners, markerIds, parameters, rejectedCandidates);
    
    vector<Vec3d> rvecs, tvecs;
    Mat cameraMatrix;
    Mat distortionCoeffs;
    
    cameraMatrix = (Mat_<double>(3,3) << [camMatrix[0] doubleValue], [camMatrix[1] doubleValue], [camMatrix[2] doubleValue],
                    [camMatrix[3] doubleValue], [camMatrix[4] doubleValue], [camMatrix[5] doubleValue],
                    [camMatrix[6] doubleValue], [camMatrix[7] doubleValue], [camMatrix[8] doubleValue] );
    
    distortionCoeffs = (Mat_<double>(1,5) << [distCoeffs[0] doubleValue], [distCoeffs[1] doubleValue], [distCoeffs[2] doubleValue], [distCoeffs[3] doubleValue], [distCoeffs[4] doubleValue] );
    
    
    aruco::estimatePoseSingleMarkers(corners, 0.05, cameraMatrix, distortionCoeffs, rvecs, tvecs);
    
    if(drawMarkers) {
        for(int i = 0; i < markerIds.size(); ++i) {
            aruco::drawAxis(inputImg, cameraMatrix, distortionCoeffs, rvecs[i], tvecs[i], 0.1);
        }
    }
    
    img = MatToUIImage(inputImg);
    
    id res = [NSMutableArray new];
    for (int e : markerIds) {
        [res addObject: [NSNumber numberWithInt:e]];
    }
    
    
    ArucoData *data = [ArucoData new];
    data.markerIds = res;
    data.image = img;
    return data;
}

@end
