//
//  OpenCVWrapper.h
//  OpenCV
//
//  Created by Federico Parroni on 14/04/17.
//  Copyright © 2017 Federico Parroni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArucoData.h"


@interface OpenCVWrapper : NSObject

+(NSString *) OpenCVVersionString;


+(ArucoData *) detectMarkersAndEstimatePose:(UIImage *)img ofDictionary:(int)dictionary camMatrix:(NSArray *)camMatrix distCoeffs:(NSArray *)distCoeffs shouldDraw:(bool)drawMarkers;

+(NSArray *) getDictionaries;

@end
